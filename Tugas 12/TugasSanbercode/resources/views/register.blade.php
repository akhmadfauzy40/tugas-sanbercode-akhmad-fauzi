<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name : </label> <br />
        <input type="text" name="firstname" /> <br />
        <br />
        <label>Last Name : </label> <br />
        <input type="text" name="lastname" /> <br />
        <br />
        <label>Gender : </label> <br />
        <input type="radio" name="gender" id="Male" value="Male" /><label for="Male">Male</label><br />
        <input type="radio" name="gender" id="Female" value="Female" /><label for="Female">Female</label><br />
        <input type="radio" name="gender" id="Other" value="Other" /><label for="Other">Other</label><br />
        <br />
        <label>Nationality :</label> <br />
        <select name="nationality" id="">
            <option value="Indonesian">Indonesian</option>
            <option value="American">American</option>
            <option value="British">British</option>
            <option value="Japanese">Japanese</option>
        </select><br /><br />
        <label>Language Spoken : </label> <br />
        <input type="checkbox" name="lang" id="indo" value="Bahasa Indonesia" /><label for="indo">Bahasa
            Indonesia</label><br />
        <input type="checkbox" name="lang" id="eng" value="English" /><label
            for="eng">English</label><br />
        <input type="checkbox" name="lang" id="oth" value="Other" /><label
            for="oth">Other</label><br /><br />
        <label>Bio : </label> <br />
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br />
        <br />
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>
