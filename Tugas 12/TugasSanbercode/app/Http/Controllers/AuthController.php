<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $name = strtoupper($request->firstname . " " . $request->lastname);
        return view('welcome')->with('name', $name);
    }
}
