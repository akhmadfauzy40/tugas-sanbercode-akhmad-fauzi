<?php
require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("Shaun");

echo "Name         : " . $sheep->name . "<br>"; // "shaun"
echo "Legs         : " . $sheep->legs . "<br>"; // 4
echo "Cold blooded : " . $sheep->cold_blooded . "<br>"; // "no"

echo "<br>";

$kodok = new Frog("Buduk");

echo "Name         : " . $kodok->name . "<br>"; // "shaun"
echo "Legs         : " . $kodok->legs . "<br>"; // 4
echo "Cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
echo "Jump         : " . $kodok->jump() . "<br>";

echo "<br>";

$sungkong = new Ape("Kera Sakti");
$sungkong->legs = 2;

echo "Name         : " . $sungkong->name . "<br>"; // "shaun"
echo "Legs         : " . $sungkong->legs . "<br>"; // 4
echo "Cold blooded : " . $sungkong->cold_blooded . "<br>"; // "no"
echo "Yell         : " . $sungkong->yell();