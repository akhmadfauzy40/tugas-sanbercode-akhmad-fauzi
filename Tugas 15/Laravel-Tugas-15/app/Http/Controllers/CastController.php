<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $data = DB::table('cast')->get();
        return view('dataCast', ['datas' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaCast' => 'required',
            'umurCast' => 'required'
        ]);

        DB::table('cast')->insert([
            'nama' => $request->namaCast,
            'umur' => $request->umurCast,
            'bio' => $request->bioCast
        ]);

        return redirect('/cast')->with('success', 'Cast Baru Berhasil di Simpan');
    }

    public function show($id)
    {
        $data = DB::table('cast')->where('id', $id)->first();
        return view('detailCast')->with('data', $data);
    }

    public function edit($id)
    {
        $data = DB::table('cast')->where('id', $id)->first();
        return view('updateCast')->with('data', $data);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'namaCast' => 'required',
            'umurCast' => 'required'
        ]);

        DB::table('cast')->where('id', $id)->update([
            'nama' => $request->namaCast,
            'umur' => $request->umurCast,
            'bio' => $request->bioCast
        ]);

        return redirect('/cast')->with('success', 'Data Cast Berhasil di Update');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast')->with('success', 'Data Cast Berhasil di Hapus');
    }
}
