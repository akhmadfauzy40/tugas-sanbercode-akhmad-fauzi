@extends('layouts.master');

@section('title')
    Insert Data
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h1 class="text-center">INPUT DATA CAST BARU</h1>
                    </div>
                    <div class="card-body">
                        <form action="/cast" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="namaCast" class="form-label">Nama Cast</label>
                                <input type="text" class="form-control" id="namaCast" name="namaCast"
                                    placeholder="Tom Holland">
                            </div>
                            <div class="mb-3">
                                <label for="umurCast" class="form-label">Umur Cast</label>
                                <input type="number" class="form-control" id="umurCast" name="umurCast" placeholder="17">
                            </div>
                            <div class="mb-3">
                                <label for="bioCast" class="form-label">Bio</label>
                                <input type="text" class="form-control" id="bioCast" name="bioCast"
                                    placeholder="Hmmmm">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
