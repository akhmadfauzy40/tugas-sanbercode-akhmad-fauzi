@extends('layouts.master')

@section('title')
    Update Data Cast
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h1 class="text-center">UPDATE DATA CAST</h1>
                    </div>
                    <div class="card-body">
                        <form action="/cast/{{ $data->id }}/edit" method="post">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="namaCast" class="form-label">Nama Cast</label>
                                <input type="text" class="form-control" id="namaCast" name="namaCast"
                                    value="{{ $data->nama }}">
                            </div>
                            <div class="mb-3">
                                <label for="umurCast" class="form-label">Umur Cast</label>
                                <input type="number" class="form-control" id="umurCast" name="umurCast"
                                    value="{{ $data->umur }}">
                            </div>
                            <div class="mb-3">
                                <label for="bioCast" class="form-label">Bio</label>
                                <input type="text" class="form-control" id="bioCast" name="bioCast"
                                    value="{{ $data->bio }}">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/cast" class="btn btn-secondary">Kembali</a>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal">
                            Hapus Data
                        </button>
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>

                    </div>
                    </form>

                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <form action="/cast/destroy/{{ $data->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Peringatan!!</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        ANDA YAKIN INGIN MENGHAPUS DATA INI? HAL INI TIDAK DAPAT DI
                                        KEMBALIKAN!!
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger"
                                            data-bs-dismiss="modal">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
