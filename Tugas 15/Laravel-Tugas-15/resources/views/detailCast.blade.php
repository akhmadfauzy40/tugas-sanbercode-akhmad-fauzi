@extends('layouts.master')

@section('title')
    Detail Data Cast
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h1 class="text-center">DETAIL DATA CAST</h1>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="namaCast" class="form-label">Nama Cast</label>
                            <input type="text" class="form-control" id="namaCast" name="namaCast"
                                value="{{ $data->nama }}" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="umurCast" class="form-label">Umur Cast</label>
                            <input type="number" class="form-control" id="umurCast" name="umurCast"
                                value="{{ $data->umur }}" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="bioCast" class="form-label">Bio</label>
                            <input type="text" class="form-control" id="bioCast" name="bioCast"
                                value="{{ $data->bio }}" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/cast" class="btn btn-secondary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
